﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DecipherItWebApp.Controllers;
using DecipherItWebApp.Models;
using DecipherItWebApp.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DecipherItWebApp.Tests.Controllers
{
    [TestClass]
    public class CaesarCipherControllerTest
    {
        [TestMethod]
        public void Index()
        {
            CaesarCipherController controller = new CaesarCipherController();

            ViewResult result = controller.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Model);
        }

        [TestMethod]
        public void CipherAjax()
        {
            CaesarCipherController controller = new CaesarCipherController();
            CaesarTextDocumentViewModel model = new CaesarTextDocumentViewModel();
            model.CaesarText = new CaesarText();
            model.CaesarText.ClearText = "фыуолофвлыов1212клжа1д2лвж1жд2фждывл12!!";
            model.CaesarText.TextLanguage = "Russian";
            model.CaesarText.Key = 13;
            string expected = "бзаышыбошзыо1212чшум1р2шоу1ур2бурзош12!!";
            PartialViewResult result = controller.CipherAjax(model) as PartialViewResult;
            string actual = (result.Model as CaesarTextDocumentViewModel).CaesarText.CipherText;
            Assert.AreEqual(expected, actual);
        }
    }
}
