﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryptors
{
    public class EnglishWords : Words
    {
        public EnglishWords()
        {
            _filename = "english.txt";
            _hashSetFileName = "english.hashset";
            if (CheckIfHashSetExists()) UpdateWordSet();
            else ReadHashSet();
        }
    }
}
