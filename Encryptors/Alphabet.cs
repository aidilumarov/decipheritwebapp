﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encryptors
{
    public class Alphabet
    {
        public static Dictionary<char, int> Russian = new Dictionary<char, int>()
        {
            { 'а', 1 },
            { 'б', 2 },
            { 'в', 3 },
            { 'г', 4 },
            { 'д', 5 },
            { 'е', 6 },
            { 'ё', 7 },
            { 'ж', 8 },
            { 'з', 9 },
            { 'и', 10 },
            { 'й', 11 },
            { 'к', 12 },
            { 'л', 13 },
            { 'м', 14 },
            { 'н', 15 },
            { 'о', 16 },
            { 'п', 17 },
            { 'р', 18 },
            { 'с', 19 },
            { 'т', 20 },
            { 'у', 21 },
            { 'ф', 22 },
            { 'х', 23 },
            { 'ц', 24 },
            { 'ч', 25 },
            { 'ш', 26 },
            { 'щ', 27 },
            { 'ъ', 28 },
            { 'ы', 29 },
            { 'ь', 30 },
            { 'э', 31 },
            { 'ю', 32 },
            { 'я', 33 },
        };

        public static Dictionary<char, int> English = new Dictionary<char, int>()
        {
            { 'a', 1 },
            { 'b', 2 },
            { 'c', 3 },
            { 'd', 4 },
            { 'e', 5 },
            { 'f', 6 },
            { 'g', 7 },
            { 'h', 8 },
            { 'i', 9 },
            { 'j', 10 },
            { 'k', 11 },
            { 'l', 12 },
            { 'm', 13 },
            { 'n', 14 },
            { 'o', 15 },
            { 'p', 16 },
            { 'q', 17 },
            { 'r', 18 },
            { 's', 19 },
            { 't', 20 },
            { 'u', 21 },
            { 'v', 22 },
            { 'w', 23 },
            { 'x', 24 },
            { 'y', 25 },
            { 'z', 26 },
        };
    }
}
