﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace Encryptors
{
    [Serializable]
    public class RussianWords : Words
    {
        public RussianWords()
        {
            _filename = "russian.txt";
            _hashSetFileName = "russian.hashset";
            if (CheckIfHashSetExists()) UpdateWordSet();
            else ReadHashSet();
        }
    }
}
