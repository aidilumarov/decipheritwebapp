﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Web;

namespace Encryptors
{
    public static class CaesarCipher
    {

        /// <summary>
        /// Encrypts the text using Caesar Cipher Algorithm
        /// </summary>
        /// <param name="text">Text to encrypt</param>
        /// <param name="key">Key to enc</param>
        /// <param name="lang">Source language</param>
        /// <returns>Encrypted string</returns>
        public static string Encrypt(string text, int key, SupportedLanguages lang)
        {
            Dictionary<char, int> alphabet = new Dictionary<char, int>();
            if (lang == SupportedLanguages.English) alphabet = Alphabet.English;
            else if (lang == SupportedLanguages.Russian) alphabet = Alphabet.Russian;
       

            StringBuilder output = new StringBuilder(text.Length);

            foreach (var c in text)
            {
                bool isUpper = Char.IsUpper(c);

                if (alphabet.ContainsKey(Char.ToLower(c)))
                {
                    int targetPosition = alphabet[Char.ToLower(c)] + key;
                    if (targetPosition > alphabet.Count()) targetPosition = targetPosition - alphabet.Count();
                    char targetChar = alphabet.ElementAt(targetPosition - 1).Key;
                    output.Append(isUpper ? Char.ToUpper(targetChar).ToString() : targetChar.ToString());
                    continue;
                }
                output.Append(c.ToString());
            }

            return output.ToString();
        }

        /// <summary>
        /// Decrypts the text which was encrypted using Caesar Cipher Algorithm
        /// </summary>
        /// <param name="text">Text to decrypt</param>
        /// <param name="key">Key the text was encrypted with</param>
        /// <param name="lang">Source language</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(string text, int key, SupportedLanguages lang)
        {
            Dictionary<char, int> alphabet = new Dictionary<char, int>();
            if (lang == SupportedLanguages.English) alphabet = Alphabet.English;
            else if (lang == SupportedLanguages.Russian) alphabet = Alphabet.Russian;


            StringBuilder output = new StringBuilder(text.Length);

            foreach (var c in text)
            {
                bool isUpper = Char.IsUpper(c);

                if (alphabet.ContainsKey(Char.ToLower(c)))
                {
                    int targetPosition = alphabet[Char.ToLower(c)] - key;
                    if (targetPosition <= 0)
                        targetPosition = alphabet.Count() - Math.Abs(key - alphabet[Char.ToLower(c)]);
                    char targetChar = alphabet.ElementAt(targetPosition - 1).Key;
                    output.Append(isUpper ? Char.ToUpper(targetChar).ToString() : targetChar.ToString());
                    continue;
                }
                output.Append(c.ToString());
            }

            return output.ToString();
        }

        public static string Decrypt(string text, SupportedLanguages lang)
        {
            return DecryptGuess(text, lang);
        }


        private static string DecryptGuess(string text, SupportedLanguages lang)
        {
            Dictionary<char, int> alphabet = new Dictionary<char, int>();

            Words words = null;
            if (lang == SupportedLanguages.English)
            {
                alphabet = Alphabet.English;
                words = new EnglishWords();
            }
            else if (lang == SupportedLanguages.Russian)
            {
                alphabet = Alphabet.Russian;
                words = new RussianWords();
            }

            for (int i = 1; i <= alphabet.Count(); i++)
            {
                var output = Decrypt(text, i, lang);
                var outputWords = output.Split();
                var matches = 0;
                foreach (var word in outputWords)
                {
                    if (words.WordSet.Contains(word.ToLower())) matches += 1;
                    if (matches > outputWords.Length / 4) return output;
                }
            }
            return "Could not determine the key";
        }
    }
}