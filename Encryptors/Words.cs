﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Encryptors
{
    public abstract class Words
    {
        protected static string _path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin", "WordLists");
        protected static string _localPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "WordLists");

        protected static string _filename;

        protected static string _hashSetFileName;

        public HashSet<string> WordSet;

        public void UpdateWordSet()
        {
            try
            {
                using (StreamReader file = File.OpenText(Path.Combine(_path, _filename)))
                {
                    WordSet = new HashSet<string>();
                    while (!file.EndOfStream)
                    {
                        WordSet.Add(file.ReadLine());
                    }
                }
            }
            catch (Exception e)
            {
                using (StreamReader file = File.OpenText(Path.Combine(_localPath, _filename)))
                {
                    WordSet = new HashSet<string>();
                    while (!file.EndOfStream)
                    {
                        WordSet.Add(file.ReadLine());
                    }
                }
            }
        }

        public void SaveHashSet()
        {
            try
            {
                using (Stream file = File.Open(Path.Combine(_path, _hashSetFileName), FileMode.Create))
                {
                    var binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(file, WordSet);
                }
            }
            catch (Exception e)
            {
                UpdateWordSet();
                using (Stream file = File.Open(Path.Combine(_localPath, _hashSetFileName), FileMode.Create))
                {
                    var binaryFormatter = new BinaryFormatter();
                    binaryFormatter.Serialize(file, WordSet);
                }
            }

        }

        public void ReadHashSet()
        {
            try
            {
                using (Stream file = File.Open(Path.Combine(_path, _hashSetFileName), FileMode.Open))
                {
                    var binaryFormatter = new BinaryFormatter();
                    WordSet = (HashSet<string>)binaryFormatter.Deserialize(file);
                }
            }
            catch (Exception e)
            {
                UpdateWordSet();
            }

        }

        protected bool CheckIfHashSetExists()
        {
            return File.Exists(Path.Combine(_path, _hashSetFileName));
        }
    }
}
