﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DecipherItWebApp.Models;

namespace DecipherItWebApp.ViewModels
{
    public class CaesarTextDocumentViewModel
    {
        public CaesarText CaesarText { get; set; }
        
        public Document Document { get; set; }
    }
}