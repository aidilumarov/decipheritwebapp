﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using Encryptors;

namespace DecipherItWebApp.Models
{
    public class CaesarText
    {
        [Display(Name="Clear Text")]
        public string ClearText { get; set; }

        [Display(Name="Cipher Text")]
        public string CipherText { get; set; }

        public int Key { get; set; }

        [Display(Name = "SupportedLanguages")]
        public IEnumerable<string> SupportedLanguages { get; set; }

        [Display(Name = "Text Language")]
        public string TextLanguage { get; set; }

    }
}