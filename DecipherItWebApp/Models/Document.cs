﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Microsoft.SqlServer.Server;

namespace DecipherItWebApp.Models
{
    public class Document
    {
        public string DocumentName { get; set; }

        public string DocumentFormat { get; set; }

        [Display(Name = "Document Language")]
        public string DocumentLanguage { get; set; }

        public HttpPostedFileBase File { get; set; }

    }
}