﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DecipherItWebApp.Models;
using DecipherItWebApp.ViewModels;
using Encryptors;
using Xceed.Words.NET;
using Xceed.Words;

namespace DecipherItWebApp.Controllers
{
    public class CaesarCipherController : Controller
    {
        public ActionResult Index()
        {
            CaesarText model = new CaesarText();
            model.SupportedLanguages = new List<string>()
            {
                "English", "Russian"
            };
            var viewModel = new CaesarTextDocumentViewModel();
            viewModel.CaesarText = model;

            return View(viewModel);
        }

        [HttpPost]
        public PartialViewResult CipherAjax(CaesarTextDocumentViewModel text)
        {
            SupportedLanguages lang = SupportedLanguages.Russian;
            if (text.CaesarText.TextLanguage == "English") lang = SupportedLanguages.English;
            else if (text.CaesarText.TextLanguage == "Russian") lang = SupportedLanguages.Russian;

            text.CaesarText.CipherText = Encryptors.CaesarCipher.Encrypt(text.CaesarText.ClearText, text.CaesarText.Key, lang);
            
            return PartialView("_CipherText", text);
        }

        [HttpPost]
        public ActionResult UploadAFile(CaesarTextDocumentViewModel viewModel)
        {
            var document = viewModel.Document;
            if (Request.Files.Count > 0) document.File = Request.Files[0];

            try
            {
                if (viewModel.Document.File.ContentLength > 0)
                {
                    string format;
                    if (IsSupportedFileFormat(document.File, out format))
                    {
                        string fileName = Path.GetFileName(viewModel.Document.File.FileName);
                        string path = Path.Combine(Server.MapPath("~/UploadedFiles"), fileName);
                        document.File.SaveAs(path);
                        SupportedLanguages lang = document.DocumentLanguage == "English"
                            ? SupportedLanguages.English
                            : SupportedLanguages.Russian;

                        viewModel.CaesarText = new CaesarText();
                        viewModel.CaesarText.ClearText = DecryptFile(path, format, lang);
                    }
                }
                return View(viewModel);
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View(viewModel);
            }
        }
        private bool IsSupportedFileFormat(HttpPostedFileBase file, out string format)
        {
            if (file.ContentType == "text/plain")
            {
                format = ".txt";
                return true;
            }
            else if (file.ContentType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
                format = ".docx";
                return true;
            }
            else
            {
                format = null;
                return false;
            }
        }

        private string DecryptFile(string path, string format, SupportedLanguages lang)
        {
            if (format == ".docx")
            {
                string text = ReadFromDocx(path);
                string decrypt = Encryptors.CaesarCipher.Decrypt(text, lang);
                return decrypt;
            }
            else
            {
                return Encryptors.CaesarCipher.Decrypt(ReadFromTextFile(path), lang);
            }
        }

        private string ReadFromDocx(string path)
        {
            using (var document = DocX.Load(path))
            {
                return document.Text;
            }
        }

        private string ReadFromTextFile(string path)
        {
            using (StreamReader reader = System.IO.File.OpenText(path))
            {
                return reader.ReadToEnd();
            }
        }
    }
}