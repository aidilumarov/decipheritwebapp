# DecipherIt!

This project is a simple SPA to encrypt text using Caesar Cipher. Currently, only English and Russian languages are supported. Decryptors project could be used a library reference if you need CaesarCipher Encryptor/Decryptor

## Getting Started

Open the main solution file, or, in case you are only interested in using the console version, open Decryptors project.

### Prerequisites

The project targets .NET Framework 4.6.1

## Running the tests

There are two test projects for both Web and Console versions. They are named accordingly.

## Deployment

The solution could be deployed on a Windows Server using IIS, or to Azure through Visual Studio

## Built With

* [ASP.NET MVC](http://asp.net/) - The web framework used
* [Xceed DocX](https://xceed.com/xceed-words-for-net/) - Microsoft Word file parser

## Authors

* **Aidil Umarov**

## License

This project is without license, and can redistributed, shared, modified free of any responsibilities

## Acknowledgments

* Thanks to Alexander Isaev for mentorship
* Thanks to Eliza for support

