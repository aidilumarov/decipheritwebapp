﻿using System;
using Encryptors;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Encryptors.Tests
{
    [TestClass]
    public class CaesarCipherTest
    {
        [TestMethod]
        public void TestCaesarCipherRussian()
        {
            string source = "фыуолофвлыов1212клжа1д2лвж1жд2фждывл12!!";
            int key = 13;
            string expected = "бзаышыбошзыо1212чшум1р2шоу1ур2бурзош12!!";
            string actual = CaesarCipher.Encrypt(source, key, SupportedLanguages.Russian);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(source, CaesarCipher.Decrypt(actual, key, SupportedLanguages.Russian));
        }

        [TestMethod]
        public void TestCaesarCipherEnglish()
        {
            string source = "12241asdkkk?mjfakslp;";
            int key = 23;
            string expected = "12241xpahhh?jgcxhpim;";
            string actual = CaesarCipher.Encrypt(source, key, SupportedLanguages.English);
            Assert.AreEqual(expected, actual);
            Assert.AreEqual(source, CaesarCipher.Decrypt(actual, key, SupportedLanguages.English));
        }

        [TestMethod]
        public void TestCaesarCipherRussianNoKey()
        {
            string source = "Р эптэп! гуап 32гцюнп 12412!!!";
            string expected = "Я люблю! свою 32семью 12412!!!";
            string actual = CaesarCipher.Decrypt(source, SupportedLanguages.Russian);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestCaesarCipherEnglishNoKey()
        {
            string source = "Lsa! evi csy jiipmrk xshec lsriyiwhew?";
            string expected = "How! are you feeling today honeuesdas?";
            string actual = CaesarCipher.Decrypt(source, SupportedLanguages.English);
            Assert.AreEqual(expected, actual);
        }
    }
}
